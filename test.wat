(module
  (func $square (export "square")
    (param i32) (result i32)
    get_local 0
    get_local 0
    i32.mul)

  (func $main (export "main")
    (param i32) (result i32)
    get_local 0
    i32.const 0
    i32.le_s
    if (result i32)
      i32.const 1
    else
      get_local 0
      i32.const 1
      i32.sub
      call $main
      get_local 0
      i32.mul
    end)
)
