# Mini-WASM
What's the minimal amount of cruft needed to run Webassembly?

## Raw Webassembly

- Install [wabt](https://github.com/WebAssembly/wabt) (for MacOS `brew install wabt`).
- Run `wat2wasm test.wat`.
- Open index.html
  - If you are using Chrome locally without a HTTP server, start with
    `google-chrome --allow-file-access-from-files index.html`

## C into Webassembly with Emscripten

- Install [emscripten](https://webassembly.org/getting-started/developers-guide/)
  (for MacOs `brew install emscripten` didn't work out of the box)
- `cd c`
- Run `emcc -s WASM=1 -s EXTRA_EXPORTED_RUNTIME_METHODS='["cwrap"]' -o test.js test.c`
- Run a server.

## Rust into Webassembly
- [Rust and WebAssembly](https://rustwasm.github.io/book/) has good
  instructions on getting setting up the toolchain and getting a
  running example, though I'm not sure if this qualifies as a minimial
  example.
