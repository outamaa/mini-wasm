#include <emscripten.h>

EMSCRIPTEN_KEEPALIVE
int fac(int n) {
    int acc = 1;
    for (int i = 1; i <= n; i++) {
        acc *= i;
    }
    return acc;
}
